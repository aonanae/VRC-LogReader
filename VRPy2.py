import json
import sqlite3
import os

# Define the database file
db_file = 'vrchat_log.db'

# Directory containing JSON files
json_dir = './json/'

# Connect to the SQLite database (it will be created if it doesn't exist)
conn = sqlite3.connect(db_file)
cur = conn.cursor()

# Create tables
cur.execute('''
    CREATE TABLE IF NOT EXISTS meta (
        total_lines INTEGER,
        sig TEXT,
        version TEXT,
        build TEXT,
        system TEXT
    )
''')

cur.execute('''
    CREATE TABLE IF NOT EXISTS timeline (
        id INTEGER PRIMARY KEY,
        name TEXT,
        joined TEXT,
        left TEXT,
        roomtype TEXT,
        worldid TEXT,
        instanceid TEXT
    )
''')

cur.execute('''
    CREATE TABLE IF NOT EXISTS players (
        id INTEGER PRIMARY KEY,
        timeline_id INTEGER,
        name TEXT,
        FOREIGN KEY (timeline_id) REFERENCES timeline (id)
    )
''')

cur.execute('''
    CREATE TABLE IF NOT EXISTS visits (
        id INTEGER PRIMARY KEY,
        player_id INTEGER,
        joined TEXT,
        left TEXT,
        multijoins INTEGER,
        FOREIGN KEY (player_id) REFERENCES players (id)
    )
''')

cur.execute('''
    CREATE TABLE IF NOT EXISTS stats (
        id INTEGER PRIMARY KEY,
        timeline_id INTEGER,
        warning_count INTEGER,
        error_count INTEGER,
        exception_count INTEGER,
        trigger_count INTEGER,
        event_count INTEGER,
        buffer_count INTEGER,
        udon_count INTEGER,
        FOREIGN KEY (timeline_id) REFERENCES timeline (id)
    )
''')

cur.execute('''
    CREATE TABLE IF NOT EXISTS stat_events (
        id INTEGER PRIMARY KEY,
        stats_id INTEGER,
        event_type TEXT,
        event_key TEXT,
        event_value INTEGER,
        FOREIGN KEY (stats_id) REFERENCES stats (id)
    )
''')

cur.execute('''
    CREATE TABLE IF NOT EXISTS processed_files (
        filename TEXT PRIMARY KEY
    )
''')

# Function to check if a file has been processed
def is_file_processed(filename):
    cur.execute("SELECT filename FROM processed_files WHERE filename = ?", (filename,))
    return cur.fetchone() is not None

# Function to process a single JSON file
def process_json_file(file_path):
    with open(file_path, encoding="utf-8") as f:
        data = json.load(f)

    # Insert data into meta table
    meta = data['meta']
    cur.execute('''
        INSERT INTO meta (total_lines, sig, version, build, system)
        VALUES (?, ?, ?, ?, ?)
    ''', (meta['total_lines'], meta['sig'], meta['version'], meta['build'], json.dumps(meta['system'])))

    # Insert data into timeline, players, visits, stats, and stat_events tables
    for timeline_entry in data['timeline']:
        cur.execute('''
            INSERT INTO timeline (name, joined, left, roomtype, worldid, instanceid)
            VALUES (?, ?, ?, ?, ?, ?)
        ''', (timeline_entry['name'], timeline_entry['joined'], timeline_entry['left'], timeline_entry['roomtype'], timeline_entry['worldid'], timeline_entry['instanceid']))
        
        timeline_id = cur.lastrowid
        
        for player in timeline_entry['players']:
            cur.execute('''
                INSERT INTO players (timeline_id, name)
                VALUES (?, ?)
            ''', (timeline_id, player['name']))
            
            player_id = cur.lastrowid
            
            for visit in player['visits']:
                cur.execute('''
                    INSERT INTO visits (player_id, joined, left, multijoins)
                    VALUES (?, ?, ?, ?)
                ''', (player_id, visit['joined'], visit['left'], visit['multijoins']))
        
        stats = timeline_entry['stats']
        cur.execute('''
            INSERT INTO stats (timeline_id, warning_count, error_count, exception_count, trigger_count, event_count, buffer_count, udon_count)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?)
        ''', (timeline_id, stats['warning']['count'], stats['error']['count'], stats['exception']['count'], stats['trigger']['count'], stats['event']['count'], stats['buffer']['count'], stats['udon']['count']))
        
        stats_id = cur.lastrowid
        
        for event_type in ['warning', 'error', 'exception', 'trigger', 'event', 'buffer', 'udon']:
            events = stats[event_type]['events']
            for key, value in events.items():
                cur.execute('''
                    INSERT INTO stat_events (stats_id, event_type, event_key, event_value)
                    VALUES (?, ?, ?, ?)
                ''', (stats_id, event_type, key, value))

# Process all JSON files in the directory
for filename in os.listdir(json_dir):
    if filename.endswith('.json'):
        file_path = os.path.join(json_dir, filename)
        if not is_file_processed(filename):
            process_json_file(file_path)
            cur.execute("INSERT INTO processed_files (filename) VALUES (?)", (filename,))
            conn.commit()

# Commit the changes and close the connection
conn.commit()
conn.close()