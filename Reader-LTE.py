import sqlite3
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
from collections import defaultdict
from datetime import datetime
import pytz
from pytz import timezone
import dash
from dash import Dash, html, dcc, Input, Output
import dash_bootstrap_components as dbc
import dash_admin_components as dac
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from colorsys import hsv_to_rgb

edt_tz = timezone('US/Eastern')
# Connect to the SQLite database
db_file = 'vrchat_log.db'
conn = sqlite3.connect(db_file)

# Load data into Pandas DataFrames
players_df = pd.read_sql_query('SELECT * FROM players', conn)
visits_df = pd.read_sql_query('SELECT * FROM visits', conn)
timeline_df = pd.read_sql_query('SELECT * FROM timeline', conn)

cur = conn.cursor()

# Query to get the most recent instances and player visits
cur.execute("""
SELECT
    t.instanceid,
    t.name,
    t.roomtype,
    t.worldid,
    p.name AS player_name,
    v.joined AS visit_joined,
    v.left AS visit_left
FROM
    timeline t
    JOIN players p ON t.id = p.timeline_id
    JOIN visits v ON p.id = v.player_id
WHERE
    v.joined >= DATE('now', '-10 day')
ORDER BY
    v.joined DESC
""")

instances = defaultdict(lambda: {
    'name': '',
    'roomtype': '',
    'worldid': '',
    'players': defaultdict(list),
    'latest_visit': None
})

for row in cur.fetchall():
    instance_id, instance_name, room_type, world_id, player_name, visit_joined, visit_left = row
    instances[instance_id]['name'] = instance_name
    instances[instance_id]['roomtype'] = room_type
    instances[instance_id]['worldid'] = world_id
    instances[instance_id]['players'][player_name].append({'joined': visit_joined, 'left': visit_left})
    if instances[instance_id]['latest_visit'] is None or visit_joined > instances[instance_id]['latest_visit']:
        instances[instance_id]['latest_visit'] = visit_joined
# Close the connection
conn.close()

# Filter out the user "AonaNae"
players_df = players_df[players_df['name'] != 'AonaNae']

# Set dark mode template
template = 'plotly_dark'

# Create visualizations
player_frequency = players_df['name'].value_counts().reset_index(name='frequency')
fig1_bar = px.bar(player_frequency.head(20), x='name', y='frequency', title='Top 20 Most Frequent Players (excluding AonaNae)', template=template)
fig1_pie = px.pie(player_frequency.head(20), values='frequency', names='name', title='Top 20 Most Frequent Players (excluding AonaNae)', template=template)

visits_df['visit_duration'] = (pd.to_datetime(visits_df['left']) - pd.to_datetime(visits_df['joined'])).dt.total_seconds() / 3600.0
player_visit_durations = visits_df.groupby('player_id')['visit_duration'].sum().reset_index()
player_visit_durations = player_visit_durations.merge(players_df[['id', 'name']], left_on='player_id', right_on='id', how='left')
player_visit_durations = player_visit_durations[['name', 'visit_duration']].groupby('name').sum().reset_index().sort_values(by='visit_duration', ascending=False)
fig2_bar = px.bar(player_visit_durations.head(10), x='name', y='visit_duration', title='Top 10 Players by Total Time Spent (in hours, excluding AonaNae)', template=template)
fig2_pie = px.pie(player_visit_durations.head(10), values='visit_duration', names='name', title='Top 10 Players by Total Time Spent (in hours, excluding AonaNae)', template=template)

fig3_pie = px.pie(player_frequency.head(30), values='frequency', names='name', title='Player Frequency (Top 30, excluding AonaNae)', hover_data=['name'], labels={'name': 'Player Name'}, template=template)
fig3_pie.update_traces(textposition='inside', textinfo='percent+label')

world_frequency = timeline_df['name'].value_counts().reset_index(name='frequency')
fig4_bar = px.bar(world_frequency.head(20), x='name', y='frequency', title='Top 20 Most Frequently Used Worlds', template=template)
fig4_pie = px.pie(world_frequency.head(20), values='frequency', names='name', title='Top 20 Most Frequently Used Worlds', template=template)

timeline_df['visit_duration'] = (pd.to_datetime(timeline_df['left']) - pd.to_datetime(timeline_df['joined'])).dt.total_seconds() / 3600.0
world_visit_durations = timeline_df.groupby('name')['visit_duration'].sum().reset_index()
fig5_bar = px.bar(world_visit_durations.sort_values(by='visit_duration', ascending=False).head(20), x='name', y='visit_duration', title='Top 20 Worlds by Total Time Spent (in hours)', template=template)
fig5_pie = px.pie(world_visit_durations.sort_values(by='visit_duration', ascending=False).head(20), values='visit_duration', names='name', title='Top 20 Worlds by Total Time Spent (in hours)', template=template)

def create_instance_details(instance_id, instance):
    players_details = []
    for player_name, visits in instance['players'].items():
        #visits_str = ", ".join([f"Joined: {visit['joined']} / Left: {visit['left']}" for visit in visits])
        #players_details.append(html.Li(f"{player_name}: {visits_str}", style={'color': '#ffffff'}))
        varx23 = 0

    instance_details = html.Div([
        html.H2(f"{instance['name']}", style={'color': '#ffffff'}),
        html.Ul([
            html.Li(f"World ID: {instance['worldid']}", style={'color': '#ffffff'}),
            html.Li(f"Room Type: {instance['roomtype']}", style={'color': '#ffffff'}),
            html.Li(html.Ul(players_details, style={'list-style-type': 'none', 'padding-left': '20px'}), style={'color': '#ffffff'}),
        ], style={'list-style-type': 'none', 'padding-left': '20px'}),
        dcc.Graph(figure=create_player_graph(instance), style={'height': '600px'})
    ])
    return instance_details

def generate_bright_colors(n):
    """Generate a list of n bright unique colors with good separation."""
    if n <= 10:
        # Use a predefined set of bright colors for a small number of players
        colors = [
            "#FF0000", "#00FF00", "#0000FF", "#FFFF00", "#FF00FF",
            "#00FFFF", "#FF8000", "#8000FF", "#00FF80", "#FF0080"
        ][:n]
    else:
        # Use HSV colormap for a larger number of players
        cmap = plt.get_cmap('hsv', n)
        colors = [mcolors.rgb2hex(cmap(i)) for i in range(cmap.N)]
    return colors

def create_player_graph(instance):
    player_data = []
    earliest_joined = {}

    # Extract player data and keep track of the earliest joined timestamp
    for player_name, visits in instance['players'].items():
        for visit in visits:
            joined_timestamp = pd.to_datetime(visit['joined']).tz_convert('America/New_York')
            left_timestamp = pd.to_datetime(visit['left']).tz_convert('America/New_York')
            player_data.append({
                'Player': player_name,
                'Action': 'Joined',
                'Timestamp': joined_timestamp,
                'End': left_timestamp
            })

            # Track the earliest joined timestamp for each player
            if player_name not in earliest_joined or joined_timestamp < earliest_joined[player_name]:
                earliest_joined[player_name] = joined_timestamp

    # Sort players based on the earliest joined timestamp
    player_order = sorted(earliest_joined, key=earliest_joined.get)

    # Sort player_data based on player_order
    player_data = sorted(player_data, key=lambda x: player_order.index(x['Player']))

    # Generate a bright custom color palette for the number of players
    num_players = len(instance['players'])
    bright_colors = generate_bright_colors(num_players)

    fig = px.timeline(player_data, x_start="Timestamp", x_end="End", y="Player", color="Player",
                      color_discrete_sequence=bright_colors, title="Player Activity")
    
    fig.update_layout(plot_bgcolor='#333333', paper_bgcolor='#333333', font_color='#ffffff')

    # Update y-axis category order to match the player_order in reverse
    fig.update_yaxes(categoryorder='array', categoryarray=player_order[::-1])

    return fig


# Create a list to store instance details
instance_details_list = []

# Loop through instances and create instance details
for instance_id, instance in instances.items():
    instance_details_list.append(create_instance_details(instance_id, instance))
    
def format_tab_label(instance_name, visit_time):
    return instance_name

# Create a list to store instance details tabs
instance_tabs = []

# Loop through instances and create tabs (up to 7)
instances = list(instances.items())[:7]  # Limit to 7 instances
for instance_id, instance in instances:
    instance_details = create_instance_details(instance_id, instance)
    tab_label = format_tab_label(instance['name'], instance['latest_visit'])
    instance_tabs.append(dcc.Tab(label=tab_label, children=[instance_details], style={'padding': '0.5rem', 'fontSize': '0.8rem'}))


def generate_visualization(data, visualization_type, template):
    """
    Generate a visualization based on the specified type.

    Args:
    data (DataFrame): The data for visualization.
    visualization_type (str): The type of visualization to generate. Supported types: 'fig1_bar', 'fig1_pie', 'fig2_bar', 'fig2_pie', 'fig3_pie', 'fig4_bar', 'fig4_pie', 'fig5_bar', 'fig5_pie'.
    template (str): The Plotly template to use.

    Returns:
    fig (plotly.graph_objs._figure.Figure): The generated Plotly figure.
    """
    if visualization_type in ['fig1_bar', 'fig1_pie']:
        player_frequency = data['name'].value_counts().reset_index(name='frequency').head(20)
        title = 'Top 20 Most Frequent Players (excluding AonaNae)'
        if visualization_type == 'fig1_bar':
            fig = px.bar(player_frequency, x='name', y='frequency', title=title, template=template)
        else:
            fig = px.pie(player_frequency, values='frequency', names='name', title=title, template=template)
    elif visualization_type in ['fig2_bar', 'fig2_pie']:
        data['visit_duration'] = (pd.to_datetime(data['left']) - pd.to_datetime(data['joined'])).dt.total_seconds() / 3600.0
        player_visit_durations = data.groupby('player_id')['visit_duration'].sum().reset_index()
        player_visit_durations = player_visit_durations.merge(players_df[['id', 'name']], left_on='player_id', right_on='id', how='left')
        player_visit_durations = player_visit_durations[['name', 'visit_duration']].groupby('name').sum().reset_index().sort_values(by='visit_duration', ascending=False).head(10)
        title = 'Top 10 Players by Total Time Spent (in hours, excluding AonaNae)'
        if visualization_type == 'fig2_bar':
            fig = px.bar(player_visit_durations, x='name', y='visit_duration', title=title, template=template)
        else:
            fig = px.pie(player_visit_durations, values='visit_duration', names='name', title=title, template=template)
    elif visualization_type == 'fig3_pie':
        player_frequency = data['name'].value_counts().reset_index(name='frequency').head(30)
        fig = px.pie(player_frequency, values='frequency', names='name', title='Player Frequency (Top 30, excluding AonaNae)', hover_data=['name'], labels={'name': 'Player Name'}, template=template)
        fig.update_traces(textposition='inside', textinfo='percent+label')
    elif visualization_type in ['fig4_bar', 'fig4_pie']:
        world_frequency = data['name'].value_counts().reset_index(name='frequency').head(20)
        title = 'Top 20 Most Frequently Used Worlds'
        if visualization_type == 'fig4_bar':
            fig = px.bar(world_frequency, x='name', y='frequency', title=title, template=template)
        else:
            fig = px.pie(world_frequency, values='frequency', names='name', title=title, template=template)
    elif visualization_type in ['fig5_bar', 'fig5_pie']:
        data['visit_duration'] = (pd.to_datetime(data['left']) - pd.to_datetime(data['joined'])).dt.total_seconds() / 3600.0
        world_visit_durations = data.groupby('name')['visit_duration'].sum().reset_index().sort_values(by='visit_duration', ascending=False).head(20)
        title = 'Top 20 Worlds by Total Time Spent (in hours)'
        if visualization_type == 'fig5_bar':
            fig = px.bar(world_visit_durations, x='name', y='visit_duration', title=title, template=template)
        else:
            fig = px.pie(world_visit_durations, values='visit_duration', names='name', title=title, template=template)
    else:
        raise ValueError("Unsupported visualization type.")

    return fig
'''
def get_recent_file_stats(limit=3):
    conn = sqlite3.connect(db_file)
    cur = conn.cursor()

    # Get the last 3 processed files
    cur.execute("SELECT filename FROM processed_files ORDER BY filename DESC LIMIT ?", (limit,))
    recent_files = [row[0] for row in cur.fetchall()]

    # Get statistics for the recent files
    file_stats = []
    for filename in recent_files:
        cur.execute("SELECT name, joined, left, roomtype, worldid, instanceid FROM timeline WHERE name = ?", (filename[:-5],))
        timeline_info = cur.fetchone()

        cur.execute("""
            SELECT
                (SELECT COUNT(*) FROM players WHERE timeline_id = t.id) AS player_count,
                (SELECT SUM(visit_duration) FROM visits v JOIN players p ON v.player_id = p.id WHERE p.timeline_id = t.id) AS total_visit_duration
            FROM timeline t
            WHERE t.name = ?
        """, (filename[:-5],))
        stats_info = cur.fetchone()

        file_stats.append({
            'filename': filename,
            'timeline_info': timeline_info,
            'player_count': stats_info[0],
            'total_visit_duration': stats_info[1]
        })

    conn.close()
    return file_stats
'''
# Create Dash app
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.DARKLY], suppress_callback_exceptions=True)

# Define app layout using dash-admin-components
app.layout = dac.Page(
    [
        # Update the Navbar to include the Toggle Chart Button
        dac.Navbar(
            color="dark",
            text="VRChat Visualization Dashboard",
            children=[
                dbc.Button("Toggle Chart Type", id="toggle-button", color="primary", className="ml-auto")
            ]
        ),

        dac.Sidebar(
            [
                dac.SidebarMenu(
                    [
                        dac.SidebarMenuItem(id='menu-item-home', label='Home', icon='home'),
                        dac.SidebarMenuItem(id='menu-item5', label='Instance Details', icon='clock'),
                        dac.SidebarMenuItem(id='menu-item1', label='Player Frequency', icon='bar-chart'),
                        dac.SidebarMenuItem(id='menu-item2', label='Time Spent', icon='clock'),
                        dac.SidebarMenuItem(id='menu-item3', label='World Usage', icon='globe'),
                        dac.SidebarMenuItem(id='menu-item4', label='Player Details', icon='user'),
                    ]
                )
            ],
            title='Dashboard',
            skin="dark"
        ),
        dac.Body(
            [
                dac.TabItems(
                    [
                        dac.TabItem(id='content-item-home', children=[
                            html.H4('Recent Activity Overview', style={'color': 'white'}),
                            html.H5('Last 3 Processed Files', style={'color': 'white'}),
                            #html.Div(id='recent-file-stats')
                        ]),
                        dac.TabItem(id='content-item1', children=[
                            dcc.Graph(id='fig1', figure=fig1_bar)  # Add the player frequency bar chart here
                        ]),
                        dac.TabItem(id='content-item2', children=[
                            dcc.Graph(id='fig2')
                        ]),
                        dac.TabItem(id='content-item3', children=[
                            dcc.Graph(id='fig4'),
                            dcc.Graph(id='fig5')
                        ]),
                        dac.TabItem(id='content-item4', children=[
                            html.Div([
                                html.H5('Enter Player Name:', style={'color': 'white'}),
                                dcc.Input(id='player-input', type='text', placeholder='Player Name', value='', style={'backgroundColor': '#333', 'color': 'white'}),
                                html.Div(id='player-details'),
                                dbc.Button("Toggle Top Worlds", id="toggle-top-worlds", color="primary", className="mb-3")
                            ])
                        ]),
                        dac.TabItem(id='content-item5', children=[
                            dcc.Tabs(instance_tabs, style={'height': '40px'})
                        ]),
                    ]
                )
            ],
            style={'backgroundColor': '#222'}
        )
    ]
)

'''
@app.callback(
    Output('recent-file-stats', 'children'),
    Input('content-item-home', 'n_clicks')
)
def update_recent_file_stats(_):
    recent_stats = get_recent_file_stats()
    stats_div = []

    for file_stat in recent_stats:
        filename = file_stat['filename']
        timeline_info = file_stat['timeline_info']
        player_count = file_stat['player_count']
        total_visit_duration = file_stat['total_visit_duration']

        stats_div.append(html.Div([
            html.H6(f'File: {filename}', style={'color': 'white'}),
            html.P(f'World Name: {timeline_info[0]}', style={'color': 'white'}),
            html.P(f'Joined: {timeline_info[1]}', style={'color': 'white'}),
            html.P(f'Left: {timeline_info[2]}', style={'color': 'white'}),
            html.P(f'Room Type: {timeline_info[3]}', style={'color': 'white'}),
            html.P(f'World ID: {timeline_info[4]}', style={'color': 'white'}),
            html.P(f'Instance ID: {timeline_info[5]}', style={'color': 'white'}),
            html.P(f'Player Count: {player_count}', style={'color': 'white'}),
            html.P(f'Total Visit Duration: {total_visit_duration:.2f} hours', style={'color': 'white'}),
            html.Hr()
        ]))

    return stats_div
'''
# Callback function to toggle between bar and pie charts
@app.callback(
    [Output('fig1', 'figure'),
     Output('fig2', 'figure'),
     Output('fig4', 'figure'),
     Output('fig5', 'figure')],
    [Input('toggle-button', 'n_clicks')]
)
def toggle_visualization(n_clicks):
    ctx = dash.callback_context
    if ctx.triggered[0]['prop_id'] == 'reload-button.n_clicks':
        raise PreventUpdate

    if n_clicks is None or n_clicks % 2 == 0:
        return fig1_bar, fig2_bar, fig4_bar, fig5_bar
    else:
        return fig1_pie, fig2_pie, fig4_pie, fig5_pie

# Callback function to display player details
@app.callback(
    [Output('player-details', 'children'),
     Output('toggle-top-worlds', 'children')],
    [Input('player-input', 'value'),
     Input('toggle-top-worlds', 'n_clicks')]
)
def display_player_details(player_name, n_clicks):
    if player_name:
        player_data = players_df[players_df['name'] == player_name]
        if not player_data.empty:
            player_ids = player_data['id'].values
            total_visits = visits_df[visits_df['player_id'].isin(player_ids)].shape[0]
            total_duration = visits_df[visits_df['player_id'].isin(player_ids)]['visit_duration'].sum()

            # Get top 10 worlds for the player with relative frequency and time spent
            player_visits = visits_df[visits_df['player_id'].isin(player_ids)].merge(players_df[['id', 'timeline_id']], left_on='player_id', right_on='id', how='left')
            player_visits = player_visits.merge(timeline_df[['id', 'name']], left_on='timeline_id', right_on='id', how='left')
            top_worlds_by_visits = player_visits.groupby('name')['id'].count().reset_index().sort_values(by='id', ascending=False).head(10)
            top_worlds_by_visits['percentage'] = (top_worlds_by_visits['id'] / total_visits) * 100
            top_worlds_by_time = player_visits.groupby('name')['visit_duration'].sum().reset_index().sort_values(by='visit_duration', ascending=False).head(10)

            # Calculate cumulative time spent over time for all players
            all_players_time = visits_df.merge(players_df[['id', 'name']], left_on='player_id', right_on='id', how='left')
            all_players_time = all_players_time.groupby(['name', pd.to_datetime(all_players_time['left']).dt.date])['visit_duration'].sum().reset_index()

            # Get the top 5 players by total time spent (including the selected player)
            top_players_time = all_players_time.groupby('name')['visit_duration'].sum().reset_index().sort_values(by='visit_duration', ascending=False).head(5)
            top_players_time = top_players_time['name'].tolist()

            if player_name not in top_players_time:
                top_players_time = [player_name] + top_players_time[:4]

            # Filter the cumulative time spent data for top players
            top_players_data = all_players_time[all_players_time['name'].isin(top_players_time)]

            # Check which mode is currently active (visit frequency or time spent)
            if n_clicks is None or n_clicks % 2 == 0:
                top_worlds = top_worlds_by_visits
                top_worlds = top_worlds.merge(top_worlds_by_time[['name', 'visit_duration']], on='name', how='left')
                mode = "Visit Frequency"
                button_text = "Show Top Worlds by Time Spent"
            else:
                top_worlds = top_worlds_by_time
                top_worlds = top_worlds.merge(top_worlds_by_visits[['name', 'id']], on='name', how='left')
                mode = "Time Spent"
                button_text = "Show Top Worlds by Visit Frequency"

            # Create pie chart for top 10 worlds with annotations
            pie_chart = go.Figure(data=[go.Pie(
                labels=[f"{name} ({time_spent:.2f} hours)" for name, time_spent in zip(top_worlds['name'], top_worlds['visit_duration'])],
                values=top_worlds['id'] if 'id' in top_worlds.columns else top_worlds['visit_duration'],
                textinfo='label+percent',
                hoverinfo='label+percent+value',
                text=[f"{name} ({percent:.2f}%, {time_spent:.2f} hours)" for name, percent, time_spent in zip(top_worlds['name'], top_worlds['percentage'] if 'percentage' in top_worlds.columns else top_worlds['visit_duration'], top_worlds['visit_duration'])]
            )])
            pie_chart.update_layout(title_text=f'Top 10 Worlds by {mode}', template=template)

            # Create line graph for cumulative time spent compared to other top players
            fig_line = px.line(
                top_players_data,
                x='left',
                y='visit_duration',
                color='name',
                title=f'Cumulative Time Spent by Top Players',
                template=template,
                line_shape='spline'  # Change the line shape to 'spline' for a smoother look
            )
            # Customize line graph appearance
            fig_line.update_layout(
                xaxis_title='Date',
                yaxis_title='Cumulative Time Spent (hours)',
                legend_title='Player Name',
                font=dict(size=12, color='white'),
                plot_bgcolor='rgba(0,0,0,0)',
                paper_bgcolor='rgba(0,0,0,0)'
            )

            fig_line.update_traces(
                mode='lines',  # Remove markers by setting mode to 'lines'
                line=dict(width=3)
            )

            details = [
                html.P(f'Player Name: {player_name}', style={'color': 'white'}),
                html.P(f'Total Visits: {total_visits}', style={'color': 'white'}),
                html.P(f'Total Time Spent (hours): {total_duration:.2f}', style={'color': 'white'}),
                dcc.Graph(figure=pie_chart),
                dcc.Graph(figure=fig_line)
            ]

            return details, button_text

    return [], "Show Top Worlds by Visit Frequency"

# Sidebar callback to update the main content based on menu selection
@app.callback(
    Output('content-item-home', 'style'),
    Output('content-item1', 'style'),
    Output('content-item2', 'style'),
    Output('content-item3', 'style'),
    Output('content-item4', 'style'),
    Output('content-item5', 'style'),
    Input('menu-item-home', 'n_clicks'),
    Input('menu-item1', 'n_clicks'),
    Input('menu-item2', 'n_clicks'),
    Input('menu-item3', 'n_clicks'),
    Input('menu-item4', 'n_clicks'),
    Input('menu-item5', 'n_clicks')
)
def update_content(n_home, n1, n2, n3, n4, n5):
    ctx = dash.callback_context

    if not ctx.triggered:
        return {'display': 'block'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}

    button_id = ctx.triggered[0]['prop_id'].split('.')[0]

    if button_id == 'menu-item-home':
        return {'display': 'block'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}
    elif button_id == 'menu-item1':
        return {'display': 'none'}, {'display': 'block'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}
    elif button_id == 'menu-item2':
        return {'display': 'none'}, {'display': 'none'}, {'display': 'block'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}
    elif button_id == 'menu-item3':
        return {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {'display': 'block'}, {'display': 'none'}, {'display': 'none'}
    elif button_id == 'menu-item4':
        return {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {'display': 'block'}, {'display': 'none'}
    elif button_id == 'menu-item5':
        return {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {'display': 'block'}
    else:
        return {'display': 'block'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}

# Run the app
if __name__ == '__main__':
    app.run_server(debug=True)
